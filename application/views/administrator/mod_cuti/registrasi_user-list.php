            <div class="col-xs-12">  
              <div class="box">
                <div class="box-header">
                  <h3 class="box-title">Semua Cuti</h3>
                  <a class='pull-right btn btn-primary btn-sm' href='<?php echo base_url().$this->uri->segment(1); ?>/tambah_cuti'>Tambahkan Data</a>
                </div><!-- /.box-header -->
                <div class="box-body">
                  <table id="example1" class="table table-bordered table-striped">
                    <thead>
                      <tr>
                        <th style='width:20px'>No</th>
                        <th>Nama Pegawai</th>
                        <th>Tanggal Cuti</th>
                        <th>Status</th>
                        <th style='width:75px'>Action</th>
                      </tr>
                    </thead>
                    <tbody>
                  <?php 
                    $no = 1;
                     
                    foreach ($record as $row){
                    $exx = explode('-',$row['tgl_mulai_cuti']);
                          $exy = explode('-',$row['tgl_selesai_cuti']);
                          $mulai = $exx[1].'/'.$exx[2].'/'.$exx[0];
                          $selesai = $exy[1].'/'.$exy[2].'/'.$exy[0];
                          $tanggalmulaiselesai = $mulai.' - '.$selesai;
                    $tgl_posting = tgl_indo($row['tanggal']);
                    if ($row['status']=='Y'){ $status = '<span style="color:green">Published</span>'; }else{ $status = '<span style="color:red">Unpublished</span>'; }
                    echo "<tr><td>$no</td>
                              <td>$row[nama]</td>
                              <td>$tanggalmulaiselesai </td>
                              <td>$status</td>
                              <td><center>
                               <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/laporan_pdf/$row[id_informasi_cuti]'><span class='glyphicon glyphicon-print'></span></a>
                               
                                <a class='btn btn-success btn-xs' title='Edit Data' href='".base_url().$this->uri->segment(1)."/edit_cuti/$row[id_informasi_cuti]'><span class='glyphicon glyphicon-edit'></span></a>
                                <a class='btn btn-danger btn-xs' title='Delete Data' href='".base_url().$this->uri->segment(1)."/delete_cuti/$row[id_informasi_cuti]' onclick=\"return confirm('Apa anda yakin untuk hapus Data ini?')\"><span class='glyphicon glyphicon-remove'></span></a>
                              </center></td>
                          </tr>";
                      $no++;
                    }
                  ?>
                  </tbody>
                </table>
              </div>