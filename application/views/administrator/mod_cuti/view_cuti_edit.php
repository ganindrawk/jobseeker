<?php 
$val_admin =1;
if ($this->session->level=='admin'){

    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Edit Cuti</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/edit_cuti',$attributes); 
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value='$rows[id_informasi_cuti]'>
                    <input type='hidden' name='status_atasan' value='1'>
                    <input type='hidden' name='pdfcuti_pemohon' value='$rows[id_informasi_cuti]_pdfcuti_pemohon.pdf'>
                    <tr><th scope='row'>NIP</th>              <td><input type='text' class='form-control' name='nip' value='$rows[nip]'></td></tr>
                    <tr><th width='120px' scope='row'>Nama Pegawai</th>    <td><input type='text' class='form-control' name='nama' value='$rows[nama]' required></td></tr>
                    
                    <tr><th scope='row'>Jabatan</th>          <td><input type='text' class='form-control' name='jabatan' value='$rows[jabatan]''></td></tr>
                     <tr><th scope='row'>Unit Kerja</th>          <td><input type='text' class='form-control' name='unit_kerja' value='$rows[unit_kerja]''></td></tr>
                     <tr><th scope='row'>Masa Kerja</th>          <td><input type='text' class='form-control' name='masa_kerja' value='$rows[masa_kerja]''></td></tr>


                    <tr><th scope='row'>Jenis Cuti</th>               <td><select name='jenis_cuti' class='form-control' required>";

                      foreach ($record as $row){
                        if ($rows['jenis_cuti'] == $row['id_jenis_cuti']){
                          echo "<option value='$row[jenis_cuti]' selected>$row[jenis_cuti]</option>";
                        }else{
                          echo "<option value='$row[id_jenis_cuti]'>$row[jenis_cuti]</option>";
                        }
                        //var_dump($row);die;
                       //echo $rows['jenis_cuti'];
                      }

                    echo "</td></tr>";
                    echo "<tr><th scope='row'>Alasan Cuti</th>             <td><textarea class='form-control' name='alasan_cuti' style='height:160px' required>$rows[alasan_cuti]</textarea></td>
                          </tr>
                          <tr><th scope='row'>Lama Cuti</th>          <td><input type='text' class='form-control' name='lama_cuti' value='$rows[lama_cuti]''></td>
                          </tr>";
                          // variabel untuk tanggal rangepicker

                          $exx = explode('-',$rows['tgl_mulai_cuti']);
                          $exy = explode('-',$rows['tgl_selesai_cuti']);
                          $mulai = $exx[1].'/'.$exx[2].'/'.$exx[0];
                          $selesai = $exy[1].'/'.$exy[2].'/'.$exy[0];
                          $tanggalmulaiselesai = $mulai.' - '.$selesai;

                          // end

                    echo "
                           <tr><th scope='row'>Tgl <small>s/d</small> Selesai</th><td><input type='text' class='form-control' id='rangepicker' name='f' value='$tanggalmulaiselesai'></td></tr>

                          <tr><th scope='row'>Sisa Cuti</th>          <td><input type='text' class='form-control' name='sisa_cuti' value='$rows[sisa_cuti]''></td>
                          </tr>
                          <tr><th scope='row'>Alamat Selama Cuti</th>             <td><textarea class='form-control' name='alamat_saat_cuti' style='height:160px' required>$rows[alamat_saat_cuti]</textarea></td>
                          </tr>
                           <tr><th scope='row'>Telephone</th>          <td><input type='text' class='form-control' name='telp' value='$rows[telp]''></td>
                          </tr>
                  
                  </tbody>
                  </table>
                </div>
              
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Setujui</button>
                    <a href='".base_url().$this->uri->segment(1)."/cuti'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                    
                  </div>
            </div></div></div>";
            echo form_close();
}
else {

  echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Edit Cuti</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/edit_cuti',$attributes); 
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value='$rows[id_informasi_cuti]'>
                    <tr><th scope='row'>NIP</th>              <td><input type='text' class='form-control' name='nip' value='$rows[nip]'></td></tr>
                    <tr><th width='120px' scope='row'>Nama Pegawai</th>    <td><input type='text' class='form-control' name='nama' value='$rows[nama]' required></td></tr>
                    
                    <tr><th scope='row'>Jabatan</th>          <td><input type='text' class='form-control' name='jabatan' value='$rows[jabatan]''></td></tr>
                     <tr><th scope='row'>Unit Kerja</th>          <td><input type='text' class='form-control' name='unit_kerja' value='$rows[unit_kerja]''></td></tr>
                     <tr><th scope='row'>Masa Kerja</th>          <td><input type='text' class='form-control' name='masa_kerja' value='$rows[masa_kerja]''></td></tr>


                    <tr><th scope='row'>Jenis Cuti</th>               <td><select name='jenis_cuti' class='form-control' required>";

                      foreach ($record as $row){
                        if ($rows['jenis_cuti'] == $row['id_jenis_cuti']){
                          echo "<option value='$row[jenis_cuti]' selected>$row[jenis_cuti]</option>";
                        }else{
                          echo "<option value='$row[id_jenis_cuti]'>$row[jenis_cuti]</option>";
                        }
                        //var_dump($row);die;
                       //echo $rows['jenis_cuti'];
                      }

                    echo "</td></tr>";
                    echo "<tr><th scope='row'>Alasan Cuti</th>             <td><textarea class='form-control' name='alasan_cuti' style='height:160px' required>$rows[alasan_cuti]</textarea></td>
                          </tr>
                          <tr><th scope='row'>Lama Cuti</th>          <td><input type='text' class='form-control' name='lama_cuti' value='$rows[lama_cuti]''></td>
                          </tr>";
                          // variabel untuk tanggal rangepicker

                          $exx = explode('-',$rows['tgl_mulai_cuti']);
                          $exy = explode('-',$rows['tgl_selesai_cuti']);
                          $mulai = $exx[1].'/'.$exx[2].'/'.$exx[0];
                          $selesai = $exy[1].'/'.$exy[2].'/'.$exy[0];
                          $tanggalmulaiselesai = $mulai.' - '.$selesai;

                          // end

                    echo "
                           <tr><th scope='row'>Tgl <small>s/d</small> Selesai</th><td><input type='text' class='form-control' id='rangepicker' name='f' value='$tanggalmulaiselesai'></td></tr>

                          <tr><th scope='row'>Sisa Cuti</th>          <td><input type='text' class='form-control' name='sisa_cuti' value='$rows[sisa_cuti]''></td>
                          </tr>
                          <tr><th scope='row'>Alamat Selama Cuti</th>             <td><textarea class='form-control' name='alamat_saat_cuti' style='height:160px' required>$rows[alamat_saat_cuti]</textarea></td>
                          </tr>
                           <tr><th scope='row'>Telephone</th>          <td><input type='text' class='form-control' name='telp' value='$rows[telp]''></td>
                          </tr>
                  
                  </tbody>
                  </table>
                </div>
              
              <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Update</button>
                    <a href='".base_url().$this->uri->segment(1)."/cuti'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                    
                  </div>
            </div></div></div>";
            echo form_close();


}
