<html>
<style>
#table.garis_tabel tr td {
border: 1px #ccc;
font-size: 10px;

}
</style>
<body>
<p>
Jakarta, 25 September 2018
<br/>
<br/>
Kepada, <br/>
Yth. Plt. Kepala Dinas Komunikasi, Informatika, dan Statistik<br/>
Melalui Kasubag Umum dan Kepegawaian<br/>
Di<br/>
Jakarta
</p>
<p>
<div style="text-align:center;">
<h3>FORMULIR PERMINTAAN DAN PEMBERIAN CUTI</h3>
</div>
</p>
<div style="font-size: 12px;">
<?php 
echo "
<table style=border-collapse:collapse width=500 border=0.5>
  <tr>
    <td colspan=8>I. DATA PEGAWAI</td>
  </tr>
  <tr>
    <td colspan=2>Nama</td>
    <td colspan=2>$rows[nama]</td>
    <td colspan=2>NIP</td>
    <td colspan=2>$rows[nip]</td>
  </tr>
  <tr>
    <td colspan=2>Jabatan</td>
    <td colspan=2>$rows[jabatan]</td>
    <td colspan=2>Masa Kerja</td>
    <td colspan=2>$rows[masa_kerja]</td>
  
  </tr>
  <tr>
    <td colspan=2>Unit Kerja</td>
    <td colspan=6>$rows[unit_kerja]</td>
  </tr>
</table>
<br/>
<table style='border-collapse:collapse' width=500 border=0.5>
  <tr>
    <td colspan=8>II. JENIS CUTI YANG DIAMBIL **</td>
  </tr>
  <tr>
    <td colspan=2>1. Cuti Tahunan</td>
    <td colspan=2>&nbsp;</td>
    <td colspan=2>2. Cuti Besar</td>
    <td colspan=2>&nbsp;</td>
  </tr>
  <tr>
    <td colspan=2>3. Cuti Sakit</td>
    <td colspan=2>&nbsp;</td>
    <td colspan=2>4. Cuti Melahirkan</td>
    <td colspan=2>&nbsp;</td>
  </tr>
  <tr>
    <td colspan=2>5. Cuti Karena Alasan Penting</td>
    <td colspan=2>&nbsp;</td>
    <td colspan=2>6. Cuti di Luar Tanggungan Negara</td>
    <td colspan=2>&nbsp;</td>
  </tr>
</table>

<br/>
<table style='border-collapse:collapse' width=500 border=0.5>
  <tr>
    <td colspan=8>III. ALASAN CUTI</td>
  </tr>
  <tr>
    <td colspan=8>$rows[alasan_cuti]</td>
  </tr>
 
</table>

<br/>
<table style='border-collapse:collapse' width=500 border=0.5>
  <tr>
    <td colspan=8>IV. LAMANYA CUTI **</td>
  </tr>
  <tr>
    <td>Selama</td>
    <td>&nbsp; (hari/bulan/tahun)*</td>
    <td>Mulai Tanggal</td>
    <td colspan=2></td>
    <td>s/d</td>
    <td colspan=2></td>
  </tr>

</table>

<br/>
<table style='border-collapse:collapse' width=500 border=0.5>
  <tr>
    <td colspan=8>V. CATATAN CUTI ***</td>
  </tr>
  <tr>
    <td colspan=4>1. CUTI TAHUNAN</td>
    <td colspan=3>2. CUTI BESAR</td>
    <td></td>
   </tr>
   <tr>
    <td>Tahun</td>
    <td>Sisa</td>
    <td colspan=2>Keterangan</td>
    <td colspan=3>3. CUTI SAKIT</td>
    <td></td>
  </tr>
  <tr>
  	<td>N-2</td>
  	<td></td>
	<td colspan=2></td>
	<td colspan=3>4. CUTI MELAHIRKAN</td>
	<td></td>
  </tr>
  <tr>
  	<td>N-1</td>
  	<td></td>
	<td colspan=2></td>
	<td colspan=3>5. CUTI KARENA ALASAN PENTING</td>
	<td></td>
  </tr>
  <tr>
  	<td></td>
  	<td></td>
	<td colspan=2></td>
	<td colspan=3>6. CUTI DI LUAR TANGGUNGAN NEGARA</td>
	<td></td>
  </tr>
  </table>

  <br/>
  <table style='border-collapse:collapse' width=500 border=0.5>
  <tr>
    <td colspan=8>VI. ALAMAT SELAMA MENJALANKAN CUTI</td>
  </tr>
  <tr>
    <td colspan=4></td>
    <td colspan=3>TELP</td>
    <td>$rows[telp]</td>
  </tr>
  <tr>
    <td colspan=6>$rows[alamat_saat_cuti]</td>
    <td colspan=2>Hormat Saya,<br/><br/><br/><br/>($rows[nama])<br/>NIP:$rows[nip]</td>
  </tr>
 
  </table>

  <br/>

  <table style='border-collapse:collapse' width=500 border=0.5>
  <tr>
	<td colspan=8>VII. PERTIMBANGAN ATASAN LANGSUNG **</td>
  </tr>
  <tr>
  	<td colspan=2>DISETUJUI</td>
  	<td colspan=2>PERUBAHAN ****</td>
  	<td colspan=2>DITANGGUHKAN ****</td>
  	<td colspan=2>TIDAK DISETUJUI ****</td>
  </tr>
  <tr>
  	<td colspan=2></td>
  	<td colspan=2></td>
  	<td colspan=2></td>
  	<td colspan=2>Kasi SIM PKM<br/><br/><br/><br/>(Rachmat Setiawan)<br/>NIP. 198102025008041009</td>
  </tr>
  </table>

 <br/>
  
  <table style='border-collapse:collapse' width=500 border=0.5>
  <tr>
	<td colspan=8>VIII. KEPUTUSAN PEJABAT YANG BERWENANG MEMBERIKAN CUTI **</td>
  </tr>
  <tr>
  	<td colspan=2>DISETUJUI</td>
  	<td colspan=2>PERUBAHAN ****</td>
  	<td colspan=2>DITANGGUHKAN ****</td>
  	<td colspan=2>TIDAK DISETUJUI ****</td>
  </tr>
  <tr>
  	<td colspan=2></td>
  	<td colspan=2></td>
  	<td colspan=2></td>
  	
  	<td colspan=2>Sekretaris Dinas,<br/><br/><br/>(Netti Herawati)<br/>NIP. 198810241993035004</td>
  </tr>
  </table>
";

//print_r($rows); 

?>

<br/>
  * Coret yang tidak perlu<br/>
  ** Pilih salah satu dengan memberi tanda centang (v)<br/>
  *** Diisi oleh pejabat yang menangani bidang kepegawaian sebelum PNS mengajukan cuti<br/>
  **** Diberi tanda centang dan alasannya<br/>
  N : Cuti tahun berjalan, N- 1 = Sisa cuti 1 tahun sebelumnya, N-2 = Sisa cuti 2 tahun sebelumnya
</div>
</body>
</html>