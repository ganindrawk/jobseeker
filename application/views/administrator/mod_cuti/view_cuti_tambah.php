<?php 
    echo "<div class='col-md-12'>
              <div class='box box-info'>
                <div class='box-header with-border'>
                  <h3 class='box-title'>Tambah Cuti</h3>
                </div>
              <div class='box-body'>";
              $attributes = array('class'=>'form-horizontal','role'=>'form');
              echo form_open_multipart($this->uri->segment(1).'/tambah_cuti',$attributes); 
          echo "<div class='col-md-12'>
                  <table class='table table-condensed table-bordered'>
                  <tbody>
                    <input type='hidden' name='id' value=''>
                    <tr><th width='120px' scope='row'>NIP</th>    <td><input type='text' class='form-control' name='nip' placeholder='NIP' required></td>
                    </tr>
                    <tr><th scope='row'>Nama Pegawai</th>              <td><input type='text' class='form-control' name='nama' placeholder='Nama Pegawai'></td>
                    </tr>
                    <tr><th scope='row'>Jabatan</th>          <td><input type='text' class='form-control' name='jabatan' placeholder='Jabatan'></td>
                    </tr>
                    <tr><th scope='row'>Unit Kerja</th>          <td><input type='text' class='form-control' name='unit_kerja' placeholder='Unit Kerja'></td>
                    </tr>
                    <tr><th scope='row'>Masa Kerja</th>          <td><input type='text' class='form-control' name='masa_kerja' placeholder='Masa Kerja'></td>
                    </tr>
                    <tr><th scope='row'>Jenis Cuti</th>               <td><select name='jenis_cuti' class='form-control' required>
                                                                            <option value='' selected>- Pilih Jenis Cuti -</option>";
                                                                            foreach ($record as $row){
                                                                                echo "<option value='$row[id_jenis_cuti]'>$row[jenis_cuti]</option>";
                                                                            }
                    echo "</td></tr>";
                    echo " <tr><th scope='row'>Alasan Cuti</th>             <td><textarea class='form-control' name='alasan_cuti' style='height:60px' required></textarea></td>
                          </tr>
                          <tr><th scope='row'>Lama Cuti</th>          <td><input type='text' class='form-control' name='lama_cuti' placeholder='Lama Cuti'></td>
                          </tr>
                          <tr><th scope='row'>Tanggal Mulai <small>s/d</small> Selesai Cuti</th><td><input type='text' class='form-control' id='rangepicker' name='mulai_cuti' placeholder='Klik Disini untuk menampilkan Tanggal'></td>
                          </tr>
                           <tr><th scope='row'>Sisa Cuti</th>          <td><input type='text' class='form-control' name='sisa_cuti' placeholder='Sisa Cuti'></td>
                          </tr>
                          <tr><th scope='row'>Alamat Selama Cuti</th>             <td><textarea class='form-control' name='alamat_saat_cuti' style='height:60px' required></textarea></td>
                          </tr>
                          <tr><th scope='row'>Telephone</th>          <td><input type='text' class='form-control' name='telp' placeholder='Telephone'></td>
                    </tr>

                  </tbody>
                  </table>
                </div>
              
                  <div class='box-footer'>
                    <button type='submit' name='submit' class='btn btn-info'>Tambahkan</button>
                    <a href='".base_url().$this->uri->segment(1)."/cuti'><button type='button' class='btn btn-default pull-right'>Cancel</button></a>
                    
                  </div>
            </div></div></div>";
            echo form_close();
