<?php

namespace Esign;

use listUser\listUser;
use registrasiUser\registrasiUser;
use signDokumen\signDokumen;
use sendSignRequest\sendSignRequest;

class Esign
{
    public function __construct()
    {

    }

    public function listUser()
    {
        require_once "API/listUser.php";
        $listUser = new listUser();
        return $listUser->index();
        // echo "listuser";
    }

    public function registrasiUser()
    {
        $error_ktp = "";
        $error_suratrekom = "";
        $error_sk = "";
        $error_imagettd = "";

        require_once "API/registrasiUser.php";
        $registrasiUser = new registrasiUser();

        $ktp_name = $_FILES["ktp"]["name"];
        $ktp_type = $_FILES["ktp"]["type"];
        $ktp_size = $_FILES["ktp"]["size"];
        $ktp_tmp = $_FILES["ktp"]["tmp_name"];

        $srt_rekom_name = $_FILES["suratrekom"]["name"];
        $srt_rekom_type = $_FILES["suratrekom"]["type"];
        $srt_rekom_size = $_FILES["suratrekom"]["size"];
        $srt_rekom_tmp = $_FILES["suratrekom"]["tmp_name"];

        $sk_name = $_FILES["skpengangkat"]["name"];
        $sk_type = $_FILES["skpengangkat"]["type"];
        $sk_size = $_FILES["skpengangkat"]["size"];
        $sk_tmp = $_FILES["skpengangkat"]["tmp_name"];

        $img_ttd_name = $_FILES["imagettd"]["name"];
        $img_ttd_type = $_FILES["imagettd"]["type"];
        $img_ttd_size = $_FILES["imagettd"]["size"];
        $img_ttd_tmp = $_FILES["imagettd"]["tmp_name"];

        $_SESSION['imgArrayFile'][] = $_FILES['ktp']; //Your file informations
        $_SESSION['obj_image_session'][] = !empty($_FILES['ktp']['tmp_name']) ? file_get_contents($_FILES['ktp']['tmp_name']) : null;

        // echo $_SESSION['imgArrayFile']['0']['name'];die();

        // =============================================================
        // Panggil dari Library
        $registrasiUser->getKtp($ktp_name, $ktp_type, $ktp_size, $ktp_tmp);
        $registrasiUser->getsrtRekom($srt_rekom_name, $srt_rekom_type, $srt_rekom_size, $srt_rekom_tmp);
        $registrasiUser->getSk($sk_name, $sk_type, $sk_size, $sk_tmp);
        $registrasiUser->getImgTtd($img_ttd_name, $img_ttd_type, $img_ttd_size, $img_ttd_tmp);

        $error_ktp = $registrasiUser->message['getKtp'];
        $error_suratrekom = $registrasiUser->message['getsrtRekom'];
        $error_sk = $registrasiUser->message['getSk'];
        $error_imagettd = $registrasiUser->message['getImgTtd'];


        $error_arr =array (
            'error_ktp' => $error_ktp,
            'error_suratrekom' => $error_suratrekom,
            'error_sk' => $error_sk,
            'error_imagettd' => $error_imagettd,
        );

        $_SESSION['error_message'] = $error_arr;

        return $registrasiUser->index();
    }

    public function sendSignRequest($path = null){
        $file = "";

        require_once "API/sendSignRequest.php";
        $sendSignRequest = new sendSignRequest();

        // $file_name = $_FILES["file"]["name"];
        // $file_type = $_FILES["file"]["type"];
        // $file_size = $_FILES["file"]["size"];
        // $file_tmp = $_FILES["file"]["tmp_name"];

         $file_name = "1_pdfcuti_pemohon.pdf";
        $file_type = "application/pdf";
        $file_size = "";
        $file_tmp = "C:/xampp/htdocs/swarakalibata/pdfcuti_pemohon/1_pdfcuti_pemohon.pdf";

        $sendSignRequest->getFile($file_name, $file_type, $file_size, $file_tmp);

        $error_file = $sendSignRequest->message['getFile'];

        $err_arr = array(
            'error_file' => $error_file
        );

       // $_SESSION = $err_arr;

        return $sendSignRequest->index();



    }

     public function signDokumen(){

        require_once "API/signDokumen.php";
        $signDokumen = new signDokumen();

        return $signDokumen->index();
    }

    public function downloadDok(){

        require_once "API/downloadDokumen.php";
        $downloadDok = new downloadDokumen();

        return $downloadDok->index();
    }


}
